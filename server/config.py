#TOKEN = "1079522803:AAF9oIZga7lggZyefQrjQK89PhbrmFGeowU"
from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin

SECRET = "secret_key"
JWT_SECRET_KEY = "jwt_secret_key"
SQLALCHEMY_DATABASE_URI = "sqlite:///test.db"
APISPEC_SPEC = APISpec(
    title="my swagger",
    version="v1",
    openapi_version="2.0",
    plugins=[MarshmallowPlugin()]
)
BOT_KEY = "bot_key"
