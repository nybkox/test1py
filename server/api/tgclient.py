from flask_apispec import MethodResource, marshal_with, use_kwargs
from webargs import fields

from server.models.tgclient import TGClient
from server.models.schemas import TGClientSchema


class TGClientResource(MethodResource):

    @marshal_with(TGClientSchema(many=True))
    def get(self):
        return TGClient.query.all()

    @use_kwargs({"tg_cid": fields.Int()})
    @marshal_with(TGClientSchema)
    def post(self, tg_cid):
        tgclient = TGClient(tg_cid=tg_cid)
        tgclient.save()
        return tgclient


class TGClientResourceWithId(MethodResource):

    @marshal_with(TGClientSchema())
    def get(self, tgclient_id):
        return TGClient.query.filter_by(id=tgclient_id).first_or_404()
