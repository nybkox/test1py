from flask import make_response
from flask_apispec import use_kwargs
from flask_jwt_extended import create_access_token
from webargs import fields

from server.config import BOT_KEY


@use_kwargs({"bot_key": fields.Str()})
def register_bot(bot_key):
    if bot_key == BOT_KEY:
        access_token = create_access_token(identity="bot")
        return make_response({"access_token": access_token}, 200)
    return make_response({"error": "Bad key"}, 401)


register_bot.provide_automatic_options = False
