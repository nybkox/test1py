from flask_apispec import MethodResource, marshal_with, use_kwargs
from flask_jwt_extended import jwt_required
from webargs import fields

from server.models.product import Product
from server.models.schemas import ProductSchema


class ProductResource(MethodResource):

    @marshal_with(ProductSchema(many=True))
    def get(self):
        return Product.query.all()

    # @jwt_required
    @use_kwargs({"name": fields.Str(validate=lambda s: 64 >= len(s) > 0),  # or marshmallow's built-in validators?
                 "description": fields.Str(validate=lambda s: 256 >= len(s) >= 0, required=False),
                 "category_id": fields.Int()})
    @marshal_with(ProductSchema)
    def post(self, category_id, name, description=""):
        product = Product(name=name, description=description, category_id=category_id)
        product.save()
        return product


class ProductResourceWithId(MethodResource):

    @marshal_with(ProductSchema)
    def get(self, product_id):
        return Product.query.filter_by(id=product_id).first_or_404()

    # @jwt_required
    @use_kwargs({"name": fields.Str(validate=lambda s: 64 >= len(s) > 0),  # or marshmallow's built-in validators?
                 "description": fields.Str(validate=lambda s: 256 >= len(s) >= 0)})
    @marshal_with(ProductSchema)
    def put(self, product_id, **kwargs):
        product = Product.query.filter_by(id=product_id).first_or_404()
        for (k, v) in kwargs.items():
            setattr(product, k, v)
        product.save()
        return product

    # @jwt_required
    @marshal_with(ProductSchema)
    def delete(self, product_id):
        product = Product.query.filter_by(id=product_id).first_or_404()
        product.remove()
        return product
