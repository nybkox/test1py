from flask import Blueprint

from server.extensions import docs
from server.api.tgclient import TGClientResourceWithId, TGClientResource
from server.api.product import ProductResourceWithId, ProductResource
from server.api.category import CategoryResourceWithId, CategoryResource
from server.api.order import OrderResourceWithId, OrderResource
from server.api.auth import register_bot

api_bp = Blueprint("api", __name__)


api_bp.add_url_rule("/tgclient/<int:tgclient_id>",
                    view_func=TGClientResourceWithId.as_view("TGClientResourceWithId"))
api_bp.add_url_rule("/tgclient",
                    view_func=TGClientResource.as_view("TGClientsResource"))
api_bp.add_url_rule("/product/<int:product_id>",
                    view_func=ProductResourceWithId.as_view("ProductResourceWithId"))
api_bp.add_url_rule("/product",
                    view_func=ProductResource.as_view("ProductResource"))
api_bp.add_url_rule("/category/<int:category_id>",
                    view_func=CategoryResourceWithId.as_view("CategoryResourceWithId"))
api_bp.add_url_rule("/category",
                    view_func=CategoryResource.as_view("CategoryResource"))
api_bp.add_url_rule("/order/<int:order_id>",
                    view_func=OrderResourceWithId.as_view("OrderResourceWithId"))
api_bp.add_url_rule("/order",
                    view_func=OrderResource.as_view("OrderResource"))
api_bp.add_url_rule("/register_bot", view_func=register_bot, methods=["POST"])


def register_resources():
    docs.register(TGClientResource, blueprint="api", endpoint="TGClientsResource")
    docs.register(TGClientResourceWithId, blueprint="api", endpoint="TGClientResourceWithId")
    docs.register(ProductResource, blueprint="api", endpoint="ProductResource")
    docs.register(ProductResourceWithId, blueprint="api", endpoint="ProductResourceWithId")
    docs.register(CategoryResource, blueprint="api", endpoint="CategoryResource")
    docs.register(CategoryResourceWithId, blueprint="api", endpoint="CategoryResourceWithId")
    docs.register(OrderResource, blueprint="api", endpoint="OrderResource")
    docs.register(OrderResourceWithId, blueprint="api", endpoint="OrderResourceWithId")
    docs.register(register_bot, blueprint="api")
