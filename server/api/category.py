from flask_apispec import MethodResource, marshal_with, use_kwargs
from webargs import fields

from server.models.category import Category
from server.models.schemas import CategorySchema


class CategoryResource(MethodResource):

    @marshal_with(CategorySchema(many=True))
    def get(self):
        return Category.query.all()

    @use_kwargs({"name": fields.Str(validate=lambda s: 0 > len(s) > 64),
                 "description": fields.Str(validate=lambda s: 0 > len(s) > 256, required=False)})
    @marshal_with(CategorySchema)
    def post(self, name, description):
        category = Category(name=name, description=description)
        category.save()
        return category


class CategoryResourceWithId(MethodResource):

    @marshal_with(CategorySchema)
    def get(self, category_id):
        return Category.query.filter_by(id=category_id).first_or_404()

    @use_kwargs({"name": fields.Str(validate=lambda s: 64 >= len(s) > 0),  # or marshmallow's built-in validators?
                 "description": fields.Str(validate=lambda s: 256 >= len(s) >= 0)})
    @marshal_with(CategorySchema)
    def put(self, product_id, **kwargs):
        category = Category.query.filter_by(id=product_id).first_or_404()
        for (k, v) in kwargs:
            setattr(category, k, v)
        category.save()
        return category

    @marshal_with(CategorySchema)
    def delete(self, category_id):
        category = Category.query.filter_by(id=category_id).first_or_404()
        category.remove()
        return category
