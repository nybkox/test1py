from flask_apispec import MethodResource, marshal_with, doc, use_kwargs
from flask_jwt_extended import jwt_required
from webargs import fields

from server.models.order import Order
from server.models.schemas import OrderSchema


class OrderResource(MethodResource):

    @marshal_with(OrderSchema(many=True))
    def get(self):
        return Order.query.all()

    # @jwt_required
    @use_kwargs({"tgclient_id": fields.Int(),
                 "product_id": fields.Int(),
                 "count": fields.Int(validate=lambda n: n > 0)})
    @marshal_with(OrderSchema)
    def post(self, category_id, name, description=""):
        product = Order(name=name, description=description, category_id=category_id)
        product.save()
        return product


class OrderResourceWithId(MethodResource):

    @marshal_with(OrderSchema)
    def get(self, product_id):
        return Order.query.filter_by(id=product_id).first_or_404()

    # @jwt_required
    @use_kwargs({"tgclient_id": fields.Int(),
                 "product_id": fields.Int(),
                 "count": fields.Int(validate=lambda n: n > 0)})
    @marshal_with(OrderSchema)
    def put(self, product_id, **kwargs):
        product = Order.query.filter_by(id=product_id).first_or_404()
        for (k, v) in kwargs:
            setattr(product, k, v)
        product.save()
        return product

    # @jwt_required
    @marshal_with(OrderSchema)
    def delete(self, product_id):
        product = Order.query.filter_by(id=product_id).first_or_404()
        product.remove()
        return product
