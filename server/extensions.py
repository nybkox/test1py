from flask_apispec import FlaskApiSpec
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager


db = SQLAlchemy()
ma = Marshmallow()
docs = FlaskApiSpec()
bcrypt = Bcrypt()
jwt = JWTManager()
