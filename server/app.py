from flask import Flask

from .extensions import db, ma, docs, bcrypt, jwt
from server.api.api import api_bp, register_resources


def create_app(config_filename):
    """
        An app factory
    """

    app = Flask(__name__)
    app.config.from_object(config_filename)

    register_extensions(app)
    register_blueprints(app)
    register_resources()

    with app.app_context():
        # fill_test_data()
        pass
    return app


def register_extensions(app):
    db.init_app(app)
    ma.init_app(app)
    docs.init_app(app)
    bcrypt.init_app(app)
    jwt.init_app(app)


def register_blueprints(app):
    app.register_blueprint(api_bp, url_prefix='/api')


def fill_test_data():
    import random
    from lorem_text import lorem

    db.drop_all()
    db.create_all()
    clients_cids = [random.randrange(1, 99999999) for _ in range(30)]
    cats_names = [lorem.words(random.randint(1, 3)) for _ in range(12)]
    prod_names = [lorem.words(random.randint(1, 3)) for _ in range(12)]

    import server.models as m

    for cid in clients_cids:
        db.session.add(m.TGClient(tg_cid=cid))
    for name in cats_names:
        db.session.add(m.Category(name=name, description=lorem.sentence()))
    for name in prod_names:
        ri = random.randint(1, len(cats_names))
        db.session.add(m.Product(name=name, category_id=ri, description=lorem.sentence()))
    for _ in range(25):
        ri = random.randint(1, len(prod_names))
        ci = random.randint(1, len(clients_cids))
        db.session.add(m.Order(product_id=ri, tgclient_id=ci, count=random.randint(0, 8), comment=lorem.sentence()))
    db.session.commit()
