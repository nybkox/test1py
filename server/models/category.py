from server.extensions import ma
from .base import Base, Column, String


class Category(Base):
    name = Column(String(64), nullable=False)
    description = Column(String(256))

