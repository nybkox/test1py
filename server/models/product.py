from server.models.base import Base, Column, Integer, String, ForeignKey, relationship


class Product(Base):
    name = Column(String(64), nullable=False)
    description = Column(String(256))
    category_id = Column(Integer, ForeignKey("category.id"), nullable=False)
    category = relationship("Category", lazy="joined")

