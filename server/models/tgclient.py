from server.models.base import Base, Column, Integer, relationship


class TGClient(Base):
    tg_cid = Column(Integer, nullable=False, unique=True)
    orders = relationship("Order", backref="parent", lazy="joined")
