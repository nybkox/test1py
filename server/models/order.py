from server.models.base import Base, Column, Integer, String, ForeignKey, relationship


class Order(Base):
    product_id = Column(Integer, ForeignKey("product.id"), nullable=False)
    product = relationship("Product", lazy="joined")
    tgclient_id = Column(Integer, ForeignKey("tg_client.id"), nullable=False)
    tgclient = relationship("TGClient", lazy="joined")
    count = Column(Integer, default=1, nullable=False)
    comment = Column(String, nullable=True)
