from datetime import datetime

from server.extensions import db, ma


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    created_utcdate = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    username = db.Column(db.String(64), nullable=False, unique=True)
    password = db.Column(db.String(60), nullable=False)

    def save(self):
        db.session.add(self)
        db.session.commit()

    def remove(self):
        db.session.remove(self)
        db.session.commit()


class UserSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = User
        include_fk = True
