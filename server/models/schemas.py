from marshmallow import fields

from server.extensions import ma
from server.models import *


class CategorySchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Category


class ProductSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Product
    category = fields.Nested("CategorySchema")


class OrderSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Order
    product = fields.Nested("ProductSchema")
    tgclient = fields.Nested("TGClientSchema", exclude=("orders",))


class TGClientSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = TGClient
    orders = fields.List(fields.Nested("OrderSchema"))
