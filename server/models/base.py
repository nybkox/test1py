from datetime import datetime

from server.extensions import db


Column = db.Column
Integer = db.Integer
DateTime = db.DateTime
String = db.String
ForeignKey = db.ForeignKey
relationship = db.relationship


class Base(db.Model):
    __abstract__ = True
    id = db.Column(db.Integer, primary_key=True)
    created_utcdate = db.Column(db.DateTime, nullable=False, default=datetime.utcnow())

    def save(self):
        db.session.add(self)
        db.session.commit()

    def remove(self):
        db.session.remove(self)
        db.session.commit()
