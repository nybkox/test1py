from server.app import create_app
from server import config

if __name__ == "__main__":
    app = create_app(config)
    app.run()
